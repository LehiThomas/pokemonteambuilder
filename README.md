# Pokemon Team Builder

A basic ‘Pokemon team building app’ using the graphql API available at https://beta.pokeapi.co/graphql/console/.

Users can search for pokemon and add them to a team of 6.
They can then view more info, reorder the team, or remove a pokemon.

## Installation

You can use yarn or npm to install dependencies and run

```bash
yarn
npm i
```

## How to Run

```bash
yarn start
npm start
```

Go to http://localhost:5173/
