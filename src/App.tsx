import { useState, useEffect } from 'react';
import SearchBar from '@components/SearchBar';
import PokeTeam from '@components/PokeTeam';

import { pokemon_v2_pokemon } from '@/types/pokemon.types';
import { TEAM_SIZE } from '@utils/consts';

import './App.styles.scss';

function App() {
  const [team, setTeam] = useState<pokemon_v2_pokemon[]>([]);

  useEffect(() => {
    let localTeam = localStorage.getItem('pokeTeam');
    if (localTeam) setTeam(JSON.parse(localTeam));
  }, []);

  function selectPokemon(pokemon: pokemon_v2_pokemon) {
    if (team.length < 6) {
      const newTeam = [...team, pokemon];
      setNewTeam(newTeam);
    }
  }

  function removePokemon(index: number) {
    const newTeam = [...team];
    newTeam.splice(index, 1);
    setNewTeam(newTeam);
  }

  function setNewTeam(newTeam: pokemon_v2_pokemon[]) {
    setTeam(newTeam);
    localStorage.setItem('pokeTeam', JSON.stringify(newTeam));
  }

  return (
    <div className='App'>
      <div className='search'>
        <SearchBar
          selectPokemon={selectPokemon}
          isDisabled={team.length === TEAM_SIZE}
        />
      </div>

      <div className='team'>
        <PokeTeam
          team={team}
          removePokemon={removePokemon}
          reorganizeTeam={setNewTeam}
        />
      </div>
    </div>
  );
}

export default App;
