import greyPokeball from '@assets/grey-pokeball.png';
import './EmptySlot.styles.scss';

const EmptySlot = () => (
  <div className='EmptySlot'>
    <img src={greyPokeball} alt='grey Pokeball' />
  </div>
);

export default EmptySlot;
