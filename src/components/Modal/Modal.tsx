import './Modal.styles.scss';

function Modal({
  handleModal,
  children,
}: {
  handleModal: () => void;
  children: React.ReactNode;
}) {
  return (
    <div className='modalBackground'>
      <div className='modalContainer'>
        <div className='titleCloseBtn'>
          <button
            onClick={() => {
              handleModal();
            }}
          >
            X
          </button>
        </div>
        {children}
      </div>
    </div>
  );
}

export default Modal;
