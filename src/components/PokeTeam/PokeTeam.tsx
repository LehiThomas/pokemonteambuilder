import { useEffect, useState } from 'react';
import { useLazyQuery } from '@apollo/client';
import { DragDropContext, Droppable, DropResult } from 'react-beautiful-dnd';

import { POKE_BY_ID_QUERY } from '@services/querys';

import EmptySlot from '@components/EmptySlot';
import TeamMember from '@components/TeamMember';
import Modal from '@components/Modal';
import PokemonInfo from '@components/PokemonInfo';

import './PokeTeam.styles.scss';
import { pokemon_v2_pokemon } from '@/types/pokemon.types';

function PokeTeam({
  team,
  removePokemon,
  reorganizeTeam,
}: {
  team: pokemon_v2_pokemon[];
  removePokemon: (index: number) => void;
  reorganizeTeam: (newTeam: pokemon_v2_pokemon[]) => void;
}) {
  const [modalOpen, setModalOpen] = useState(false);
  const [selectedId, setSelectedId] = useState<number | null>(null);
  const [getPokemonById, { loading, data }] = useLazyQuery(POKE_BY_ID_QUERY, {
    variables: { value: selectedId },
  });

  useEffect(() => {
    if (selectedId) {
      setModalOpen(true);
      getPokemonById();
    }
  }, [selectedId]);

  function handleModal() {
    setModalOpen(false);
    setSelectedId(null);
  }

  const onDragEnd = (result: DropResult) => {
    const { destination, source, draggableId } = result;

    if (!destination) {
      return;
    }

    if (
      destination.droppableId === source.droppableId &&
      destination.index === source.index
    ) {
      return;
    }

    const sourcePokemon = team[source.index];
    const newTeamOrder = [...team];
    newTeamOrder.splice(source.index, 1);
    newTeamOrder.splice(destination.index, 0, sourcePokemon);

    reorganizeTeam(newTeamOrder);
  };

  const displayTeam = () => {
    let pokemon = [];

    for (let i = 0; i < 6; i++) {
      if (team[i]) {
        pokemon.push(
          <TeamMember
            key={team[i].id}
            pokemon={team[i]}
            handleRemove={removePokemon}
            selectPokemon={setSelectedId}
            index={i}
          />
        );
      } else {
        pokemon.push(<EmptySlot key={i} />);
      }
    }
    return pokemon;
  };

  return (
    <div className='PokeTeam'>
      <h2>Your Pokémon Team</h2>
      <DragDropContext onDragEnd={onDragEnd}>
        <Droppable droppableId='droppable-list'>
          {(provided) => (
            <div
              ref={provided.innerRef}
              {...provided.droppableProps}
              className='PokeTeamList'
            >
              {displayTeam()}
              {provided.placeholder}
            </div>
          )}
        </Droppable>
      </DragDropContext>

      {modalOpen && (
        <Modal handleModal={handleModal}>
          <div>
            {data?.pokemon_v2_pokemon && (
              <PokemonInfo pokemon={data.pokemon_v2_pokemon[0]} />
            )}
          </div>
        </Modal>
      )}
    </div>
  );
}

export default PokeTeam;
