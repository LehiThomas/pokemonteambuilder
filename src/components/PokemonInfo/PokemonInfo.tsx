import {
  pokemon_details,
  pokemontypes,
  pokemonstats,
} from '@/types/pokemon.types';
import { getStatDisplayName } from '@utils/helpers';
import './PokemonInfo.styles.scss';

function PokemonInfo({ pokemon }: { pokemon: pokemon_details }) {
  const sprites = JSON.parse(pokemon.pokemon_v2_pokemonsprites[0].sprites);
  const stats = pokemon.pokemon_v2_pokemonstats;
  const types = pokemon.pokemon_v2_pokemontypes;

  return (
    <div className='PokemonInfo'>
      {sprites.front_default && (
        <img
          className='sprite'
          src={sprites.front_default}
          alt={`${pokemon.name}-sprite`}
        />
      )}
      <span className='pokeman-number'>#{pokemon.id}</span>
      <h1 className='pokeman-name'>{pokemon.name}</h1>

      <div className='basic-info'>
        {types.map((type: pokemontypes) => (
          <div key={type.pokemon_v2_type.name} className='type'>
            <h4>{type.pokemon_v2_type.name}</h4>
          </div>
        ))}
      </div>

      <div className='basic-info'>
        <div className='basic-info-section'>
          <h3>HEIGHT</h3>
          <span>{pokemon.height}</span>
        </div>
        <div className='basic-info-section'>
          <h3>WEIGHT</h3>
          <span>{pokemon.weight}</span>
        </div>
        <div className='basic-info-section'>
          <h3>BASE XP</h3>
          <span>{pokemon.base_experience}</span>
        </div>
      </div>

      <div className='basic-info'>
        {stats.map((stat: pokemonstats) => (
          <div key={stat.pokemon_v2_stat.name} className='basic-info-section'>
            <h4>{getStatDisplayName(stat.pokemon_v2_stat.name)}</h4>
            <span>{stat.base_stat}</span>
          </div>
        ))}
      </div>
    </div>
  );
}

export default PokemonInfo;
