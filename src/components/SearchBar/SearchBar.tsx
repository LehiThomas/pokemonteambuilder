import React, { useState, useEffect } from 'react';
import { useLazyQuery } from '@apollo/client';
import { InputText } from 'primereact/inputtext';
import { Card } from 'primereact/card';

import { POKE_SEARCH_QUERY } from '@services/querys';

import SearchItem from '@components/SearchItem';
import { pokemon_v2_pokemon } from '@/types/pokemon.types';
import pokeball from '@assets/pokeball.png';

import './SearchBar.styles.scss';

function SearchBar({
  selectPokemon,
  isDisabled,
}: {
  selectPokemon: (pokemon: pokemon_v2_pokemon) => void;
  isDisabled: boolean;
}) {
  const [value, setValue] = useState('');
  const [getPokemon, { loading, data }] = useLazyQuery(POKE_SEARCH_QUERY, {
    variables: { value: setSearchValue() },
  });

  function setSearchValue() {
    if (value === '') {
      return value;
    } else {
      return `%${value}%`;
    }
  }

  useEffect(() => {
    if (value !== '') {
      getPokemon();
    }
  }, [value]);

  useEffect(() => {
    if (isDisabled) setValue('');
  }, [isDisabled]);

  return (
    <div className='SearchBar'>
      <Card title='Search Pokémon'>
        <InputText
          className='p-inputtext-lg block'
          placeholder='pokémon name'
          value={value}
          onChange={(e) => setValue(e.target.value)}
          disabled={isDisabled}
        />
      </Card>

      <div className='results'>
        {loading ? (
          <div className='loading'>
            <img
              className='loading-icon'
              src={pokeball}
              alt='pokeball loading spinner'
            />
          </div>
        ) : (
          <div
            className={
              data?.pokemon_v2_pokemon.length > 0 ? 'results-list' : ''
            }
          >
            {data?.pokemon_v2_pokemon &&
              data.pokemon_v2_pokemon.map((pokemon: pokemon_v2_pokemon) => (
                <SearchItem
                  key={`${pokemon.id}-search-item`}
                  pokemon={pokemon}
                  handleOnClick={selectPokemon}
                />
              ))}
          </div>
        )}
      </div>
    </div>
  );
}

export default SearchBar;
