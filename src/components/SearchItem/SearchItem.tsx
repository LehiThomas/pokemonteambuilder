import { pokemon_v2_pokemon } from '@/types/pokemon.types';
import './SearchItem.styles.scss';

function SearchItem({
  pokemon,
  handleOnClick,
}: {
  pokemon: pokemon_v2_pokemon;
  handleOnClick: (pokemon: pokemon_v2_pokemon) => void;
}) {
  let sprites = JSON.parse(pokemon.pokemon_v2_pokemonsprites[0].sprites);

  return (
    <div className='SearchItem' onClick={() => handleOnClick(pokemon)}>
      <h2>{pokemon.name}</h2>
      {sprites.front_default && (
        <img src={sprites.front_default} alt={`${pokemon.name}-sprite`} />
      )}
    </div>
  );
}

export default SearchItem;
