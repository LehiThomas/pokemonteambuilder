import { Draggable } from 'react-beautiful-dnd';
import { pokemon_v2_pokemon } from '@/types/pokemon.types';
import { Button } from 'primereact/button';
import './TeamMember.styles.scss';

function TeamMember({
  pokemon,
  index,
  handleRemove,
  selectPokemon,
}: {
  pokemon: pokemon_v2_pokemon;
  index: number;
  selectPokemon: (value: number) => void;
  handleRemove: (index: number) => void;
}) {
  let sprites = JSON.parse(pokemon?.pokemon_v2_pokemonsprites[0].sprites);

  return (
    <Draggable draggableId={`${pokemon.id}-${index}`} index={index}>
      {(provided) => (
        <div
          {...provided.draggableProps}
          {...provided.dragHandleProps}
          ref={provided.innerRef}
          className='TeamMember'
        >
          <h2>{pokemon.name}</h2>
          {sprites.front_default && (
            <img src={sprites.front_default} alt={`${pokemon.name}-sprite`} />
          )}
          <div className='button-bar'>
            <Button
              label='Info'
              className='p-button-raised p-button-info'
              onClick={() => selectPokemon(pokemon.id)}
            />
            <Button
              label='Remove'
              className='p-button-raised p-button-danger'
              onClick={() => handleRemove(index)}
            />
          </div>
        </div>
      )}
    </Draggable>
  );
}

export default TeamMember;
