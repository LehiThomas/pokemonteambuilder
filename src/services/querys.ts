import { gql } from '@apollo/client';

export const POKE_BY_ID_QUERY = gql`
  query pokemon_v2_pokemon($value: Int) {
    pokemon_v2_pokemon(where: { id: { _eq: $value } }) {
      id
      name
      height
      base_experience
      weight
      pokemon_v2_pokemonstats {
        base_stat
        pokemon_v2_stat {
          name
        }
      }
      pokemon_v2_pokemontypes {
        pokemon_v2_type {
          name
        }
      }
      pokemon_v2_pokemonsprites {
        sprites
      }
    }
  }
`;

export const POKE_SEARCH_QUERY = gql`
  query pokemon_v2_pokemon($value: String) {
    pokemon_v2_pokemon(where: { name: { _nlike: "%-%", _ilike: $value } }) {
      id
      name
      pokemon_v2_pokemonsprites {
        sprites
      }
    }
  }
`;
