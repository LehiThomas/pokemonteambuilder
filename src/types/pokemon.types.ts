export interface pokemon_v2_pokemon {
  id: number;
  name: string;
  pokemon_v2_pokemonsprites: sprites[];
}

export interface sprites {
  sprites: string;
}

export interface pokemon_details {
  id: number;
  name: string;
  height: number;
  base_experience: number;
  weight: number;
  pokemon_v2_pokemonsprites: sprites[];
  pokemon_v2_pokemonstats: pokemonstats[];
  pokemon_v2_pokemontypes: pokemontypes[];
}

export interface pokemonstats {
  base_stat: number;
  pokemon_v2_stat: { name: string };
}

export interface pokemontypes {
  pokemon_v2_type: { name: string };
}
