export function getStatDisplayName(name: string) {
  let stat = '';
  switch (name) {
    case 'attack':
      stat = 'ATK';
      break;
    case 'defense':
      stat = 'DEF';
      break;
    case 'special-attack':
      stat = 'SpA';
      break;
    case 'special-defense':
      stat = 'SpD';
      break;
    case 'speed':
      stat = 'SPD';
      break;
    case 'hp':
      stat = 'HP';
      break;
    default:
      stat = name.substring(0, 3);
      break;
  }
  return stat;
}
